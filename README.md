# test2.1

Создала автотест для функции GET (Find Pets by status). Код автотеста:
```
pm.test("Status code is 200", function () {
    pm.response.to.have.status(200);
});
//проверка того что статус питомца с id=0 будет sold
pm.test("Check status == sold", function () {
    var jsonData = pm.response.json();
    pm.expect(jsonData[0].status).to.eql("sold");
});
//проверка того что статус питомца с id=0 будет available
pm.test("Check status == available", function () {
    var jsonData = pm.response.json();
    pm.expect(jsonData[0].status).to.eql("available");
});
```
Делала с помощью Postman и его встроенных функций.
